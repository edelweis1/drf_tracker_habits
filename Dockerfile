FROM python:3

WORKDIR /DRF_Tracker_habits

COPY ./requirements.txt .

RUN pip install -r requirements.txt

COPY . .